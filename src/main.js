// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './plugins/element.js'
import './css/general.css'
import VueDirectiveImagePreviewer from 'vue-directive-image-previewer'
import 'vue-directive-image-previewer/dist/assets/style.css'
import JwChat from 'jwchat';
import axios from "axios";
// axios.defaults.baseURL = 'http://localhost:8080'

Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(VueDirectiveImagePreviewer)
Vue.use(JwChat)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
