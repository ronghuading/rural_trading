import Vue from 'vue'
import VueRouter from 'vue-router'
import l_login from "../components/login/l_login";
import l_register from "../components/login/l_register";
import home from "../components/home/home";
import human from "../components/home/human";
import goods_list from "../components/list/goods_list";
import human_list from "../components/list/human_list";
import good_item from "../components/item/good_item";
import goods_list_by_type from "../components/list/goods_list_by_type";
import human_list_by_type from "../components/list/human_list_by_type";
import user_information from "../components/personal_center/user_information";
import good_list from "../components/home/good_list";
import user_avatar from "../components/personal_center/user_avatar";
import user_info from "../components/personal_center/user_info";
import user_password from "../components/personal_center/user_password";
import user_address from "../components/personal_center/user_address";
import user_message from "../components/personal_center/user_message";
import update_info from "../components/personal_center/update_info";
import set_resource from "../components/set/set_resource";
import user_resource from "../components/resource_manager/user_resource";
import all_resource from "../components/resource_manager/all_resource";
import under_audit from "../components/resource_manager/under_audit";
import fail_audit from "../components/resource_manager/fail_audit";
import history_resource from "../components/resource_manager/history_resource";
import user_focus from "../components/focus_resource/user_focus";
import complained_resource from "../components/resource_manager/complained_resource";
import manager_home from "../components/manager/manager_home";
import audit from "../components/manager/audit";
import complaint from "../components/manager/complaint";
import appeal from "../components/manager/appeal";
import user from "../components/manager/user";
import manager from "../components/manager/manager";
import work_message from "../components/manager/work_message";
import manage_resource from "../components/manager/manage_resource";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: l_login
  },
  {
    path: '/login',
    component:l_login
  },
  {
    path: '/register',
    component:l_register
  },
  {
    path: '/home',
    component:home
  },
  {
    path: '/good',
    component: good_list
  },
  {
    path: '/human',
    component:human
  },
  {
    path: '/goods_list',
    component:goods_list
  },
  {
    path: '/human_list',
    component:human_list
  },
  {
    path: '/goods_list_by_type',
    component:goods_list_by_type
  },
  {
    path: '/human_list_by_type',
    component:human_list_by_type
  },
  {
    path: '/good_item',
    component:good_item
  },
  {
    path: '/setResource',
    component: set_resource
  },
  {
    path: '/information',
    redirect: '/avatar',
    component: user_information,
    children: [
      {
        path: '/avatar',
        component: user_avatar
      },
      {
        path: '/userinfo',
        component: user_info
      },
      {
        path: '/updateInfo',
        component: update_info
      },
      {
        path: '/password',
        component: user_password
      },
      {
        path: '/address',
        component: user_address
      },
      {
        path: '/message',
        component: user_message
      }
    ]
  },
  {
    path: '/resource',
    redirect: '/allResource',
    component: user_resource,
    children: [
      {
        path: '/allResource',
        component: all_resource
      },
      {
        path: '/underAudit',
        component: under_audit
      },
      {
        path: '/failAudit',
        component: fail_audit
      },
      {
        path: '/complained',
        component: complained_resource
      },
      {
        path: '/historyResource',
        component: history_resource
      }
    ]
  },
  {
    path: '/myFocus',
    component: user_focus
  },
  {
    path: '/managerHome',
    redirect: '/audit',
    component: manager_home,
    children: [
      {
        path: '/audit',
        component: audit
      },
      {
        path: '/complaint',
        component: complaint
      },
      {
        path: '/appeal',
        component: appeal
      },
      {
        path: '/manageResource',
        component: manage_resource
      },
      {
        path: '/user',
        component: user
      },
      {
        path: '/manager',
        component: manager
      },
      {
        path: '/workMessage',
        component: work_message
      }
    ]
  },
]

const router = new VueRouter({
  routes
})
// export default new Router({
//   routes: [
//     {
//       path: '/',
//       component: l_login
//     },
//     {
//       path: '/login',
//       component:l_login
//     },
//     {
//       path: '/register',
//       component:l_register
//     },
//     {
//       path: '/home',
//       component:home
//     },
//     {
//       path: '/good',
//       component: good_list
//     },
//     {
//       path: '/human',
//       component:human
//     },
//     {
//       path: '/goods_list',
//       component:goods_list
//     },
//     {
//       path: '/human_list',
//       component:human_list
//     },
//     {
//       path: '/goods_list_by_type',
//       component:goods_list_by_type
//     },
//     {
//       path: '/human_list_by_type',
//       component:human_list_by_type
//     },
//     {
//       path: '/good_item',
//       component:good_item
//     },
//     {
//       path: '/setResource',
//       component: set_resource
//     },
//     {
//       path: '/information',
//       redirect: '/avatar',
//       component: user_information,
//       children: [
//         {
//           path: '/avatar',
//           component: user_avatar
//         },
//         {
//           path: '/userinfo',
//           component: user_info
//         },
//         {
//           path: '/updateInfo',
//           component: update_info
//         },
//         {
//           path: '/password',
//           component: user_password
//         },
//         {
//           path: '/address',
//           component: user_address
//         },
//         {
//           path: '/message',
//           component: user_message
//         }
//       ]
//     },
//     {
//       path: '/resource',
//       redirect: '/allResource',
//       component: user_resource,
//       children: [
//         {
//           path: '/allResource',
//           component: all_resource
//         },
//         {
//           path: '/underAudit',
//           component: under_audit
//         },
//         {
//           path: '/failAudit',
//           component: fail_audit
//         },
//         {
//           path: '/complained',
//           component: complained_resource
//         },
//         {
//           path: '/historyResource',
//           component: history_resource
//         }
//       ]
//     },
//     {
//       path: '/myFocus',
//       component: user_focus
//     },
//     {
//       path: '/managerHome',
//       redirect: '/audit',
//       component: manager_home,
//       children: [
//         {
//           path: '/audit',
//           component: audit
//         },
//         {
//           path: '/complaint',
//           component: complaint
//         },
//         {
//           path: '/appeal',
//           component: appeal
//         },
//         {
//           path: '/manageResource',
//           component: manage_resource
//         },
//         {
//           path: '/user',
//           component: user
//         },
//         {
//           path: '/manager',
//           component: manager
//         },
//         {
//           path: '/workMessage',
//           component: work_message
//         }
//       ]
//     },
//   ]
// })

/**
 * 定义路由导航守卫
 * 参数1 to   路由跳转的网址
 * 参数2 from 路由从哪里来
 * 参数3 next 是一个函数，表示放行或重定向
 *            next() 放行
 *            next("/login") 重定向
 * 业务实现：
 *    核心逻辑：检查是否有token
 *        有token   表示已经登录，放行请求
 *        没有token 表示用户没有登录，重定向到登录页面
 *        如果访问login页面 直接放行
 */
router.beforeEach((to,from,next) => {
  if (to.path === "/login" || to.path === "/register") {
    return next()
  }
  //说明用户访问的页面不是login 请求需要校验
  //获取token数据
  let token = window.sessionStorage.getItem("token")
  //下列if的判断 解释为：如果token不为null
  if (token) {
    if (to.path === "/managerHome" || to.path === '/audit' || to.path === '/complaint' || to.path === '/appeal' || to.path === '/manageResource' || to.path === '/user' || to.path === '/manager' || to.path === '/workMessage') {
      let user = JSON.parse(window.sessionStorage.getItem("user"))
      if (parseInt(user.managerStatus) === 1) {
        return next()
      } else {
        return next("/login")
      }
    }
    return next()
  }
  next("/login")
})

export default router
